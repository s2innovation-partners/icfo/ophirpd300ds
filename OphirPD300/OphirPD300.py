import logging
import sys
from random import uniform
from statistics import mean

from tango.server import Device, attribute, device_property, DeviceMeta, command, run
from tango import DebugIt, DispLevel, AttrWriteType, DevState
from Ophir3AQUADlib import Ophir3AQUADController, Ophir3AQUADException

debug_it = DebugIt(show_args=True, show_kwargs=True, show_ret=True)


class OphirPD300(Device):
    __metaclass__ = DeviceMeta

    # ---------------------------------
    #   Properties
    # ---------------------------------

    ReadPeriod = device_property(
        dtype=int,
        default_value=1000,
        doc="Polling period for reading data in ms for the Ophir BeamTrack sensor"
    )

    NormalizedPoints = device_property(
        dtype=int,
        default_value=1000,
        doc="Number of collected entries of power value for the Ophir BeamTrack sensor"
    )

    Simulated = device_property(
        dtype=bool,
        default_value=False,
        doc="""Data generation mode. If True data are randomly generated.
         If False data are obtained from Ophir 3A-QUAD sensor"""
    )
    IP = device_property(
        dtype="str",
        mandatory=True,
        doc="IP of the sensor"
    )

    # ---------------------------------
    #   Global methods
    # ---------------------------------

    @debug_it
    def init_device(self):
        Device.init_device(self)
        self.set_state(DevState.INIT)

        self._power = 0
        self._power_history = [0] * (self.NormalizedPoints + 1)
        self._multiplier = 1

        if not self.Simulated:
            self.sensor = Ophir3AQUADController(self.IP)
            try:
                self.sensor.connect()
                self.set_state(DevState.ON)
            except Ophir3AQUADException:
                self.error_stream(str(sys.exc_info()[1]))
                logging.error(str(sys.exc_info()[1]))
                raise sys.exc_info()[1]

    @debug_it
    def delete_device(self):
        self.sensor.disconnect()

    # ---------------------------------
    #   Attributes
    # ---------------------------------

    power = attribute(label="Power",
                      dtype=float,
                      polling_period=ReadPeriod.default_value,
                      display_level=DispLevel.OPERATOR,
                      access=AttrWriteType.READ,
                      unit="W",
                      format="%11.10f",
                      fget="get_power",
                      doc="The power value in [W] for the Ophir PD300 sensor")

    power_normal = attribute(label="Power normalized",
                             dtype=float,
                             polling_period=ReadPeriod.default_value,
                             display_level=DispLevel.OPERATOR,
                             access=AttrWriteType.READ,
                             format="%5.4f",
                             fget="get_power_normalized",
                             doc="The normalized power value for the Ophir PD300 sensor")

    multiplier = attribute(label="Multiplier",
                           dtype=float,
                           polling_period=ReadPeriod.default_value,
                           display_level=DispLevel.OPERATOR,
                           access=AttrWriteType.READ_WRITE,
                           format="%5.4f",
                           fget="get_multiplier",
                           fset='set_multiplier',
                           doc="The multiplier for calculating power value")

    @command(
        polling_period=ReadPeriod.default_value
    )
    @debug_it
    def fetch_power(self):
        """
        get power value
        """
        if self.Simulated:
            self._power = uniform(-10, 10)
        else:
            try:
                self._power = self.sensor.read_power()
            except Ophir3AQUADException:
                self.warn_stream(str(sys.exc_info()[1]))
                raise sys.exc_info()[1]

        del self._power_history[0]
        self._power_history.append(self._power)

    def calculate_normalized(self):
        avr_list = mean(self._power_history[:-1])
        if avr_list == 0:
            return 0
        return self._power / avr_list

    @debug_it
    def get_power_normalized(self):
        return self.calculate_normalized()

    @debug_it
    def set_multiplier(self, multiply):
        self._multiplier = multiply

    @debug_it
    def get_multiplier(self):
        return self._multiplier

    @debug_it
    def get_power(self):
        return self._power * self._multiplier


def main(args=None, **kwargs):
    logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=logging.DEBUG)
    return run((OphirPD300,), args=args, **kwargs)


if __name__ == '__main__':
    main()
