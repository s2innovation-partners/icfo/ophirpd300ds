# PowerMeter PD300

[LINK](https://www.ophiropt.com/laser--measurement/laser-power-energy-meters/products/Laser-Photodiode-Sensors/Standard-Photodiode-Sensors/PD300)

The PD300 is a general purpose photodiode laser measurement sensor with swivel mount and a removable filter and has a 10x10mm aperture.  Without filter, its spectral range is 350 - 1100nm and its power measuring range is 500pW - 30mW. With filter the spectral range is 430nm - 1100nm and the power range is 200µW - 300mW. It has the exclusive Ophir automatic background subtraction feature. The sensor comes with a 1.5 meter cable for connecting to a meter or PC interface.
