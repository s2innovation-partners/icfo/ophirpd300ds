import setuptools

setuptools.setup(
    name="tangods-ophirpd3000",
    version="1.0.0",
    description="Device server for ophirpd3000",
    author="S2innovation",
    author_email="contact@s2innovation.com",
    license="Proprietary",
    classifiers=[
        'License :: Other/Proprietary License'],
    packages=setuptools.find_packages(),
    install_requires=['statistics'],
    entry_points={
        'console_scripts': [
            "OphirPD3000=OphirPD300.OphirPD300:main",
        ]
    }

)

