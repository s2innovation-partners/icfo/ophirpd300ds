FROM centos:7
COPY maxiv.repo /etc/yum.repos.d/
RUN yum install -y epel-release && yum makecache
RUN yum install -y \
    python-pytango \
    python-fandango \
    python-pip \
    && yum clean all
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY OphirPD300/OphirPD300.py /
ENTRYPOINT ["python", "/OphirPD300.py"]
